import psycopg2
import pandas as pd


# create default database

def create_database():
    # connect to default database
    conn = psycopg2.connect("host = 127.0.0.1 dbname = postgres user = postgres password = postgres")
    conn.set_session(autocommit=True)
    cur = conn.cursor()

    # cur.execute("DROP DATABASE football")
    cur.execute("CREATE DATABASE football")

    cur.close()
    conn.close()

    # connect to football db
    conn = psycopg2.connect("host = 127.0.0.1 dbname = football user = postgres password = postgres")
    conn.set_session(autocommit=True)
    cur = conn.cursor()

    return cur, conn


# create tables

def create_table(create_tables_queries):
    for query in create_tables_queries:
        cur.execute(query)
        conn.commit()


# read csv files

FootballGames = pd.read_csv("data/games.csv")
Leagues = pd.read_csv("data/leagues.csv")
Teams = pd.read_csv("data/teams.csv")
Teamstats = pd.read_csv("data/teamstats.csv")

# cleaning data

FootballGames_clean = FootballGames[["gameID", "leagueID", "season", "homeTeamID", "awayTeamID"]]
Teamstats_clean = Teamstats[["gameID", "teamID", "season", "location", "goals"]]

# creating database

cur, conn = create_database()

# create tables

FootballGames_table_create = ("""CREATE TABLE IF NOT EXISTS FootballGames (gameID int PRIMARY KEY,
                             leagueID int NOT NULL,
                             season int NOT NULL,
                             homeTeamID int NOT NULL,
                             awayTeamID int NOT NULL
                             )""")

# cur.execute(FootballGames_table_create)

Teams_table_create = ("""CREATE TABLE IF NOT EXISTS Teams (teamID int PRIMARY KEY,
                             name VARCHAR(100)
                             )""")

# cur.execute(Teams_table_create)

Leagues_table_create = ("""CREATE TABLE IF NOT EXISTS Leagues (leagueID int PRIMARY KEY,
                             name VARCHAR(100),
                             understatNotation VARCHAR(50)
                             )""")

# cur.execute(Leagues_table_create)

Teamstats_table_create = ("""CREATE TABLE IF NOT EXISTS Teamstats (gameID int,
                             teamID int,
                             season int NOT NULL,
                             location VARCHAR(2),
                             goals int NOT NULL,
                             PRIMARY KEY (gameID, teamID)
                             )""")

# cur.execute(Teamstats_table_create)

create_tables_queries = [FootballGames_table_create, Teams_table_create, Leagues_table_create, Teamstats_table_create]

create_table(create_tables_queries)

# insert data into tables

FootballGames_table_insert = ("""INSERT INTO FootballGames (gameID,
leagueID,
season,
homeTeamID,
awayTeamID)
VALUES (%s, %s, %s, %s, %s)""")

for i, row in FootballGames_clean.iterrows():
    cur.execute(FootballGames_table_insert, list(row))

Leagues_table_insert = ("""INSERT INTO Leagues (leagueID,
name,
understatNotation)
VALUES (%s, %s, %s)""")

for i, row in Leagues.iterrows():
    cur.execute(Leagues_table_insert, list(row))

Teamstats_table_insert = ("""INSERT INTO Teamstats (gameID,
teamID,
season,
location,
goals)
VALUES (%s, %s, %s, %s, %s)""")

for i, row in Teamstats_clean.iterrows():
    cur.execute(Teamstats_table_insert, list(row))

Teams_table_insert = ("""INSERT INTO Teams (teamid,
name)
VALUES (%s, %s)""")

for i, row in Teams.iterrows():
    cur.execute(Teams_table_insert, list(row))

# close connection to the database

cur.close()
conn.close()
